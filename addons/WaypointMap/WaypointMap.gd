extends Spatial

const DELETE = 0
const MOVE = 1
const EXTEND = 2

signal waypoint_added
signal waypoint_removed

export var zoomSensitivity = 0.1
export var maxZoom = 2
export var minZoom = 0.8
export var panSensitivity = 0.04
export var panGestureSensitivity = 0.2
export var magnifyGestureSensitivity = 0.2
export var pixelsPerUnit = 500
export var lineThickness = 0.012
export var debugMode = false

var waypoint = preload("res://addons/WaypointMap/Waypoint.tscn")

onready var mapCamera: Camera = $"MapCamera"
onready var mapGeometry: MeshInstance = $"Map"
onready var pathGeometry: ImmediateGeometry = $"PathGeometry"
onready var contextMenu = $"ContextMenu"

var desiredX = 0
var desiredY = 0
var desiredZ = 1
var mapWidth = 1
var mapHeight = 1
var waypoints = []
var paths = []
var currentPath = null
var currentPathIndex = 0
var mousePosition
var selectedNodes = []
var previousMousePosition: Vector3
var contextMenuTargetNode: Node
var movingNode: Node

func getWaypoints():
	return waypoints

func getPaths():
	return paths

func setMapTexture(texture: Texture):
	mapGeometry.get_surface_material(0).albedo_texture = texture
	mapWidth = (texture.get_width() / pixelsPerUnit) / 2.0
	mapHeight = (texture.get_height() / pixelsPerUnit) / 2.0
	mapGeometry.scale.x = mapWidth
	mapGeometry.scale.y = mapHeight

func insertWaypoint(mapPosition: Vector3):
	if !currentPath:
		newPath()
	var newWaypoint = _setupNewWaypoint(mapPosition)
	emit_signal("waypoint_added", waypoints)
	currentPath.append(newWaypoint)
	_drawPaths()

func newPath():
	currentPath = []
	currentPathIndex = paths.size()
	paths.append(currentPath)

func endPath():
	currentPath = null
	_drawPaths()

func removeWaypoint(waypoint):
	waypoint.path.erase(waypoint)
	waypoints.erase(waypoint)
	remove_child(waypoint)
	_drawPaths()
	emit_signal("waypoint_removed", waypoints)

func _setupNewWaypoint(mapPosition: Vector3):
	var newWaypoint = waypoint.instance()
	add_child(newWaypoint)
	newWaypoint.translation = mapPosition
	newWaypoint.connect("input_event", self, "_on_node_input_event", [newWaypoint])
	newWaypoint.path = currentPath
	waypoints.append(newWaypoint)
	return newWaypoint

func _ready():
	if debugMode:
		setMapTexture(load("res://addons/WaypointMap/DebugMap.png"))
	contextMenu.set_as_minsize()

func _input(event):
	if event is InputEventMouseMotion and movingNode:
		movingNode.translation = mapCamera.project_position(event.position, mapCamera.translation.z)
		_drawPaths()
	if event is InputEventMouseButton and event.pressed:
		if event.button_index == BUTTON_WHEEL_UP:
			desiredZ -= zoomSensitivity
		if event.button_index == BUTTON_WHEEL_DOWN:
			desiredZ += zoomSensitivity
		if event.button_index == BUTTON_RIGHT and currentPath:
			endPath()
	if event is InputEventPanGesture:
		desiredX += event.delta.x * panGestureSensitivity
		desiredY -= event.delta.y * panGestureSensitivity
	if event is InputEventMagnifyGesture:
		desiredZ /= event.factor

func _process(_delta):
	if Input.is_action_pressed("map_end_path"):
		endPath()

	if Input.is_action_pressed("map_pan_left"):
		desiredX -= panSensitivity
	if Input.is_action_pressed("map_pan_right"):
		desiredX += panSensitivity
	if Input.is_action_pressed("map_pan_up"):
		desiredY += panSensitivity
	if Input.is_action_pressed("map_pan_down"):
		desiredY -= panSensitivity

	desiredX = clamp(desiredX, -mapWidth, mapWidth)
	desiredY = clamp(desiredY, -mapHeight, mapHeight)
	desiredZ = clamp(desiredZ, minZoom, maxZoom)

	mapCamera.translation = Vector3(desiredX, desiredY, desiredZ)

	if currentPath:
		_drawPaths()

func _drawPaths():
	var lastPoint
	pathGeometry.clear()
	pathGeometry.begin(Mesh.PRIMITIVE_TRIANGLES)
	for path in paths:
		lastPoint = null
		for point in path:
			if lastPoint != null:
				_drawEdge(point, lastPoint)
			lastPoint = point
	if currentPath:
		_drawEdge(currentPath.back(), mousePosition)
	pathGeometry.end()

func _extractTranslation(point):
	match typeof(point):
		TYPE_INT:
			return waypoints[point].translation
		TYPE_VECTOR3:
			return point
		TYPE_OBJECT:
			return point.translation

func _drawEdge(point, lastPoint):
	var p = _extractTranslation(point)
	var lastP = _extractTranslation(lastPoint)
	# cross is used to offset verts perpendicular to the direction of the line
	# this way all lines are the same thickness regardless of orientation
	var cross = (p - lastP).cross(Vector3.FORWARD).normalized() * lineThickness
	# UV data is set up to permit a smooth gradient from alpha 0 at the edges of lines
	# two triangles facing the camera create the line rect
	pathGeometry.set_uv(Vector2(1, 0))
	pathGeometry.add_vertex(Vector3(p.x, p.y, p.z) + cross)
	pathGeometry.set_uv(Vector2(0, 0))
	pathGeometry.add_vertex(Vector3(p.x, p.y, p.z) - cross)
	pathGeometry.set_uv(Vector2(1, 0))
	pathGeometry.add_vertex(Vector3(lastP.x, lastP.y, lastP.z) + cross)
	pathGeometry.set_uv(Vector2(1, 0))
	pathGeometry.add_vertex(Vector3(p.x, p.y, p.z) - cross)
	pathGeometry.set_uv(Vector2(1, 0))
	pathGeometry.add_vertex(Vector3(lastP.x, lastP.y, lastP.z) - cross)
	pathGeometry.set_uv(Vector2(0, 0))
	pathGeometry.add_vertex(Vector3(lastP.x, lastP.y, lastP.z) + cross)

func _on_StaticBody_input_event(_camera, event, mouse_position, _click_normal, _shape_idx):
	mousePosition = mouse_position
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		insertWaypoint(mouse_position)

func _on_node_input_event(camera, event: InputEvent, mouse_position, _click_normal, _shape_idx, sender: Node):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_RIGHT:
		var screenPosition = camera.unproject_position(mouse_position)
		contextMenu.popup()
		contextMenu.rect_position = screenPosition
		contextMenuTargetNode = sender
		contextMenu.set_item_disabled(EXTEND, sender.path.back() != sender)


	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		if movingNode:
			movingNode = null
		elif currentPath:
			#TODO: Allow chaining paths together
			endPath()
		else:
			if selectedNodes.find(sender) == -1:
				selectedNodes.append(sender)
			# removeWaypoint(sender)


func _on_ContextMenu_id_pressed(id):
	match id:
		DELETE:
			removeWaypoint(contextMenuTargetNode)
		MOVE:
			movingNode = contextMenuTargetNode
		EXTEND:
			currentPath = contextMenuTargetNode.path

