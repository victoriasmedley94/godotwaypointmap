# 0.2

- Context menu with delete, move, and extend functionality.

# 0.1 Initial Release

- Basic zooming, panning, waypoint and path creation.
