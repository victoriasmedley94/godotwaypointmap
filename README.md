## Setup

Introduce the WaypointMap.tscn scene into the game.

## Configuration

Add input map entries for the following:

```
map_zoom_out
map_zoom_in
map_pan_left
map_pan_right
map_pan_up
map_pan_down
map_insert_waypoint
map_end_path
```
